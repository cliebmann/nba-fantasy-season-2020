"""
Doc

Input = Null
Output = line delimited json

Notes

__main__2019-19 NBA Players (per game average):
https://www.basketball-reference.com/leagues/NBA_2019_per_game.html

Game Finder - single player 2018-19 season:
https://www.basketball-reference.com/play-index/pgl_finder.cgi?request=1&match=game&year_min=2019&year_max=2019&is_playoffs=N&age_min=0&age_max=99&season_start=1&season_end=-1&pos_is_g=Y&pos_is_gf=Y&pos_is_f=Y&pos_is_fg=Y&pos_is_fc=Y&pos_is_c=Y&pos_is_cf=Y&player_id=abrinal01&order_by=date_game

Game Finder - all players 2018-19 season:
https://www.basketball-reference.com/play-index/pgl_finder.cgi?request=1&match=game&year_min=2019&year_max=2019&is_playoffs=N&age_min=0&age_max=99&season_start=1&season_end=-1&pos_is_g=Y&pos_is_gf=Y&pos_is_f=Y&pos_is_fg=Y&pos_is_fc=Y&pos_is_c=Y&pos_is_cf=Y&order_by=date_game&order_by_asc=Y

"Next page"
https://www.basketball-reference.com/play-index/pgl_finder.cgi?request=1&player_id=&match=game&year_min=2019&year_max=2019&age_min=0&age_max=99&team_id=&opp_id=&season_start=1&season_end=-1&is_playoffs=N&draft_year=&round_id=&game_num_type=&game_num_min=&game_num_max=&game_month=&game_day=&game_location=&game_result=&is_starter=&is_active=&is_hof=&pos_is_g=Y&pos_is_gf=Y&pos_is_f=Y&pos_is_fg=Y&pos_is_fc=Y&pos_is_c=Y&pos_is_cf=Y&c1stat=&c1comp=&c1val=&c1val_orig=&c2stat=&c2comp=&c2val=&c2val_orig=&c3stat=&c3comp=&c3val=&c3val_orig=&c4stat=&c4comp=&c4val=&c4val_orig=&is_dbl_dbl=&is_trp_dbl=&order_by=date_game&order_by_asc=Y&offset=100

Max offset = 26101
Pages = 262

Notebook

# note offset=0
>>> url = 'https://www.basketball-reference.com/play-index/pgl_finder.cgi?request=2&match=game&year_min=2019&year_max=2019&is_playoffs=N&age_min=0&age_max=99&season_start=1&season_end=-1&pos_is_g=Y&pos_is_gf=Y&pos_is_f=Y&pos_is_fg=Y&pos_is_fc=Y&pos_is_c=Y&pos_is_cf=Y&order_by=date_game&order_by_asc=Y&offset=0'

>>> response = requests.get(url)

>>> soup = BeautifulSoup(response.text, 'html.parser')

>>> stats_table = soup.find('table', {'id': 'stats'})

>>> player_box_score = stats_table.find('tbody').find_all('tr')[0]

>>> player_box_score.find('th').has_attr('data-stat')
True

# effectively same check as above (more complete but more expensive)
# in practice can just assume well formed (exception -> None)
>>> any(player_box_score.find_all(attrs={"data-stat": True}))
True

>>> [td['data-stat'] for td in player_box_score.find_all('td')]
['player', 'age', 'pos', 'date_game', 'team_id', 'game_location', 'opp_id', 'game_result', 'gs', 'mp', 'fg', 'fga', 'fg_pct', 'fg2', 'fg2a', 'fg2_pct', 'fg3', 'fg3a', 'fg3_pct', 'ft', 'fta', 'ft_pct', 'orb', 'drb', 'trb', 'ast', 'stl', 'blk', 'tov', 'pf', 'pts', 'game_score']

# or get_text()
>>> [td.text for td in player_box_score.find_all('td')]
['Álex Abrines', '25-076', 'G-F', '2018-10-16', 'OKC', '@', 'GSW', 'L', '0', '23', '3', '8', '.375', '1', '2', '.500', '2', '6', '.333', '0', '0', '', '0', '2', '2', '0', '0', '0', '0', '2', '8', '3.4']

>>> soup.find(text='Next page').parent['href']
'/play-index/pgl_finder.cgi?request=1&player_id=&match=game&year_min=2019&year_max=2019&age_min=0&age_max=99&team_id=&opp_id=&season_start=1&season_end=-1&is_playoffs=N&draft_year=&round_id=&game_num_type=&game_num_min=&game_num_max=&game_month=&game_day=&game_location=&game_result=&is_starter=&is_active=&is_hof=&pos_is_g=Y&pos_is_gf=Y&pos_is_f=Y&pos_is_fg=Y&pos_is_fc=Y&pos_is_c=Y&pos_is_cf=Y&c1stat=&c1comp=&c1val=&c1val_orig=&c2stat=&c2comp=&c2val=&c2val_orig=&c3stat=&c3comp=&c3val=&c3val_orig=&c4stat=&c4comp=&c4val=&c4val_orig=&is_dbl_dbl=&is_trp_dbl=&order_by=date_game&order_by_asc=Y&offset=100'
"""

from bs4 import BeautifulSoup
import urllib
import requests
import json
import time
import sys

BASKETBALL_REFERENCE_ROOT = 'https://basketball-reference.com'

# no offset  (defaults to 0)
INITIAL_RELURL = '/play-index/pgl_finder.cgi?request=1&player_id=&match=game&year_min=2019&year_max=2019&age_min=0&age_max=99&team_id=&opp_id=&season_start=1&season_end=-1&is_playoffs=N&draft_year=&round_id=&game_num_type=&game_num_min=&game_num_max=&game_month=&game_day=&game_location=&game_result=&is_starter=&is_active=&is_hof=&pos_is_g=Y&pos_is_gf=Y&pos_is_f=Y&pos_is_fg=Y&pos_is_fc=Y&pos_is_c=Y&pos_is_cf=Y&c1stat=&c1comp=&c1val=&c1val_orig=&c2stat=&c2comp=&c2val=&c2val_orig=&c3stat=&c3comp=&c3val=&c3val_orig=&c4stat=&c4comp=&c4val=&c4val_orig=&is_dbl_dbl=&is_trp_dbl=&order_by=date_game&order_by_asc=Y'

TARGET_FIELDS = [
    ('player', str), ('pos', str), ('date_game', str), ('team_id', str),
    ('mp', int), ('fg', int), ('fga', int), ('fg_pct', float),
    ('fg3', int), ('fg3a', int), ('fg3_pct', float), ('ft', int),
    ('fta', int), ('ft_pct', float), ('trb', int), ('ast', int), ('stl', int),
    ('blk', int), ('tov', int), ('pts', int)
]

def stats_url(relurl):
    return urllib.parse.urljoin(BASKETBALL_REFERENCE_ROOT, relurl)

# Given 'player game finder' results page, parse 'stats' and 'next page'
# Returns 2-tuple of stats and relative url
def parse_page(html):
    def safe_cast(cast_func, val):
        if val:
            return cast_func(val)
        else:
            return cast_func()

    def extract_stats(row):
        try:
            return {field: safe_cast(cast_func, row.find('td', {'data-stat': field}).text) \
                    for (field, cast_func) in TARGET_FIELDS}
        except Exception as e:
            # print(f'extract_stats: {e}', file=sys.stderr)
            return None

    soup = BeautifulSoup(html, 'html.parser')
   
    stats_table = soup.find('table', {'id': 'stats'})
    stats = [extract_stats(row) for row in stats_table.find_all('tr')]
  
    try:
        next_relurl = soup.find(text='Next page').parent['href']
    except Exception as e:
        print(f'parse_page: {e}', file=sys.stderr)
        next_relurl = None

    return (stats, next_relurl)

def fetch_stats():
    next_relurl = INITIAL_RELURL
    get_time_total = 0.0
    parse_time_total = 0.0
    iterations = 0
    
    while next_relurl:
        iterations += 1

        next_url = stats_url(next_relurl)
        # print(next_url, file=sys.stderr)
       
        t0 = time.perf_counter()

        response = requests.get(next_url)
        # print(response, file=sys.stderr)
       
        t1 = time.perf_counter()

        stats, next_relurl = parse_page(response.text)
       
        t2 = time.perf_counter()
        
        for stat in stats:
            if stat:
                yield stat
       
        get_time = t1 - t0
        get_time_total += get_time
        
        parse_time = t2 - t1
        parse_time_total += parse_time
       
        print(f'iterations={iterations}, get_time={get_time}, parse_time={parse_time}', file=sys.stderr)

        # if iterations == 1:
        #     break

    print(f'get_time: total={get_time_total}, avg={get_time_total/iterations}', file=sys.stderr)
    print(f'parse_time: total={parse_time_total}, avg={parse_time_total/iterations}', file=sys.stderr)

if __name__ == '__main__':
    for stat in fetch_stats():
        print(json.dumps(stat))
