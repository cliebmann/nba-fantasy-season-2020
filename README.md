# 2020 NBA Season Fantasy

## 2019 Season Game Stats
See game\_stats\_2019
And samples/all\_stats\_page.html

```
$ pipenv run python game_stats_2019/fetch_player_boxscores.py > samples/2_pages.ldjson

$ pipenv run time python game_stats_2019/fetch_player_boxscores.py > data/game_stats_2019.ldjson
iterations=1, get_time=1.1526337999985117, parse_time=1.3793908000006923
iterations=2, get_time=1.1832767000014428, parse_time=1.4117439999990893
iterations=3, get_time=3.415023800000199, parse_time=1.377942899998743
...
iterations=260, get_time=3.2352601000002323, parse_time=1.414846599998782
iterations=261, get_time=3.4194519000011496, parse_time=1.4552624999996624
parse_page: 'NoneType' object has no attribute 'parent'
iterations=262, get_time=2.5735924999989948, parse_time=0.23103500000070198
get_time: total=888.1051428999963, avg=3.3897142858778486
parse_time: total=364.0984948000114, avg=1.3896889114504252
371.46user 18.15system 20:53.76elapsed 31%CPU (0avgtext+0avgdata 47912maxresident)k
0inputs+0outputs (0major+292607minor)pagefaults 0swaps
$ head -n2 data/game_stats_2019.ldjson 
{"player": "\u00c1lex Abrines", "pos": "G-F", "date_game": "2018-10-16", "team_id": "OKC", "mp": 23, "fg": 3, "fga": 8, "fg_pct": 0.375, "fg3": 2, "fg3a": 6, "fg3_pct": 0.333, "ft": 0, "fta": 0, "ft_pct": 0.0, "trb": 2, "ast": 0, "stl": 0, "blk": 0, "tov": 0, "pts": 8}
{"player": "Steven Adams", "pos": "C", "date_game": "2018-10-16", "team_id": "OKC", "mp": 36, "fg": 6, "fga": 12, "fg_pct": 0.5, "fg3": 0, "fg3a": 0, "fg3_pct": 0.0, "ft": 5, "fta": 8, "ft_pct": 0.625, "trb": 11, "ast": 4, "stl": 2, "blk": 0, "tov": 2, "pts": 17}
$ jq -sr '(["player", "pos", "date_game", "team_id", "mp", "fg", "fga", "fg_pct", "fg3", "fg3a", "fg3_pct", "ft", "fta", "ft_pct", "trb", "ast", "stl", "blk", "tov", "pts"]) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' data/game_stats_2019.ldjson > data/game_stats_2019.csv
$ wc data/game_stats_2019.* | head -n -1
  26102   52211 2051497 data/game_stats_2019.csv
    26101 1070149 6884876 data/game_stats_2019.ldjson
$ ls -lh data/game_stats_2019.* | cut -d' ' -f5,9
2.0M data/game_stats_2019.csv
6.6M data/game_stats_2019.ldjson
```

Note the short `parse_time` for the last page (there's only one row in the stats table)
